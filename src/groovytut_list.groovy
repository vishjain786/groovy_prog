class GroovyTu {
	static void main(String[] args){
		def primes = [2,3,5,7,11,13,17];
		println("2nd prime - "+ primes[1]);

		primes << 19;
		primes = primes + [23,31];
		primes = primes - [31];

		println(primes);
	}
} 
