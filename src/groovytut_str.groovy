class GroovyTu {
	static void main(String[] args){
		def name = "vishal";

		println('I am ${name}\n'); // Single quotes are taken literally
		println("I am ${name}\n");

		def multStr = '''I am 
going to make a offer, 
you cannot refuse!''';

		println(multStr);

		println("First 3 characters of name - " + name[0..2]);
		println("Every other characters of name - " + name[0,2,4]);
		println("Length of name - " + name.length());

		def repeatName = name*2;
		println("Repeated Name - " + repeatName);
	}
} 
