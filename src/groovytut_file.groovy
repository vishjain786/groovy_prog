class GroovyTu {
	static void main(String[] args){
		new File('file.txt').eachLine{
			line->println "$line"
		}
		new File('file.txt').withWriter('utf-8') {
			writer -> writer.writeLine('Hello');
		}
		File file1 = new File('file.txt');
		file1.append("there!");

		file1.withWriter('utf-8') {
			writer -> writer.writeLine('Hello there ... 123');
		}

		// Print whole file
		println('file.txt');

		// Print file size
		println("File size - ${file1.length()}");

		// Copy to new file.
		def file2 = new File('file2.txt');
		file2.delete();
		file2 << file1.text;

	}
} 
