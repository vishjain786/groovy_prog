class GroovyTu {
	static void main(String[] args){
		def i = 0;

		while(i < 10){
			if(i%2){
				i++; continue;
			}
			if(i==8) break;
			println(i); i++
		}
		for(i=0; i<5; i++){
			println("number - " + i);
		}
		for(j in 3..7){
			println("new number - " + j);
		}
		def randList = [11,-12,19,'a',["a","b",20]];
		for(j in randList){
			println("list item - " + j);
		}
	}
} 
