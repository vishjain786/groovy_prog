class Car {
	def company;
	def year;

	def Car(comp,yr){
		this.company = comp;
		this.year = yr;
	}
	def void tell(){
		println("Company is - ${company} and year is - ${year}");
	}
	
}


class GroovyTu {
	static void main(String[] args){
		def car1 = new Car("Maruti", '2018');
		car1.tell();
	}
} 
