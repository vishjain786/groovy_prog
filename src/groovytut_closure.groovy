class GroovyTu {
	static void main(String[] args){
		def clos = {param1,param2 -> println("$param1+$param2")}
		
		clos.call(5,4);
		clos('a',1);
		func(11,clos);
	}

	def static func(num,closure){
		closure(num,9);
	}
} 
