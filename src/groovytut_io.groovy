class GroovyTu {
	static void main(String[] args){
		def randStr = "Random";
		
		println("A $randStr string");
		
		printf("%-10s %d %.2f %10s\n", [randStr, 10, 1.2345, randStr]);
		
		print("What is your name? ");
		def fName = System.console().readLine();
		println("Hello " + fName);

		print("Enter a number : ");
		def num1 = System.console().readLine().toDouble();
		
		print("Enter a number : ");
		def num2 = System.console().readLine().toDouble();

		printf("%.2f + %.2f = %.2f\n",[num1, num2, (num1+num2)]);
	}
} 
