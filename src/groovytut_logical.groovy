class GroovyTu {
	static void main(String[] args){
		def age = 6;

		if(age == 5){
			println("Yoyr age is 5!");
		}
		else if((age > 5) && (age < 8)){
			println("Yoyr age is b/w 5 and 8!");
		}
		else{
			println("Yoyr age is above 8!");
		}

		def cond = true;
		println(cond?'true':'false');

		switch(age){
			case 16:
				println("You can drive");
			case 18:
				println("You can vote");
				break;
			default: println("You can do anything");

		}
	}
} 
