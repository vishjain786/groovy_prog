class GroovyTu {
	static void main(String[] args){
		println("Hello world!");
		
		// Everything in Groovy is an object just like python
		def a = "A string";
		println("a = " + a);
		a = 32;
		println("a = " + a);
		
		println("Largest Int = " + Integer.MAX_VALUE);
		println("Smallest Int = " + Integer.MIN_VALUE);

		println("Largest Float = " + Float.MAX_VALUE);
		println("Smallest Float = " + Float.MIN_VALUE);
		
		println("Largest Double = " + Double.MAX_VALUE);
                println("Smallest Double = " + Double.MIN_VALUE);
	}
} 
