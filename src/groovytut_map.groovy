class GroovyTu {
	static void main(String[] args){
		def map = [
			'name' : 'vishal',
			'number' : 1234,
			'list' : [1,2,"3",[3,2,'1']]
		];
		println("Name - " + map['name']);
		println("Number - " + map.get('number'));

		map.put('city', 'hyd');
		println("Has city - " + map.containsKey('city'));
		
		println("Map length - " + map.size());
	}
} 
