class GroovyTu {
	static void main(String[] args){
		func();
		println("5.2 + 4.57 = " + getSum(5.2,4.57));

		def nums = sumAll(1,2,3,4);
		println("Sum = " + nums);
	}
	static def func(){
		println("hello there");
	}

	static def getSum(num1=0, num2=0){
		return num1+num2;
	}

	static def sumAll(int... num){
		def sum = 0;
		for(j in num) sum+=j;
//		num.each{sum+=it;}
		return sum;
	}
} 
