## Groovy practice 

### Pre-requisites
1. JVM >= 8.0
2. Groovy >= 2.5.4 (brew install groovy)

### Folder structure
* src/ => Here are some groovy src code files.
* files/ => text/images/etc/ for file operations.

### To run
* groovy <_.groovy-file_>
